import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/dist/query/react";

interface Option{
    id?:number;
    type?:string;
    price?:number;
    describe?:string;
    img?:string;
    label?:string;
}

export const OptionApi = createApi({
    reducerPath: 'optionApi',
    tagTypes: ['Options', 'oneOption'],
    baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_SERVER_URL + '/api/option' }),
    endpoints: (builder) => ({
        getAllOption: builder.query<Option, void>({
            query: () => '/',
            providesTags: ['Options']
        }),

        getOneOption: builder.query<Option, number>({
            query: (id) => '/' + id,
            providesTags:['oneOption']
        }),

        postOption: builder.mutation<Option, Option>({
            query: (body) => ({
                url: '/',
                method: 'POST',
                body
            }),
            invalidatesTags: ['Options']
        })
    })
});

export const {useGetAllOptionQuery, useGetOneOptionQuery, usePostOptionMutation} = OptionApi;

