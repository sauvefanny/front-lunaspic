export interface User {

    id?: number;
    name?: string;
    password?: string;
    role?: string;
    email?: string;

}

export interface Option {

    id?: number;
    type?: string;
    price?: number;
    describe?: string;
    img?: string;
    label?: string;

}

export interface Customer {

    id?: number;
    name?: string;
    firstName?: string;
    email?: string;
    phone?: string;
    birthdate?: string;
    address?: string;
    quotes?: Quote[];
}

export interface Quote {

    id?: number;
    creatDate: Date;
    processing?: number;
    priceTotal?: number;
    customer?: Customer;
    options?: Option[];
}
